Exam preparation app is the admin app for student exam preparation system.
Key features include:
- User Registration/Sign in
- Add/Edit Quiz
    - Add MCQ
    - Add Short Answered Questions
- Post articles
- Add/Edit/Remove Categories
- Add/Edit/Remove Home Page Items