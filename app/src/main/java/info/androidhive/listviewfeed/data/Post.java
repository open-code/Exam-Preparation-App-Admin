package info.androidhive.listviewfeed.data;

import android.graphics.Bitmap;
import com.exrebound.examshooteradmin.utils.Utils;

public class Post {
    private int id;
    private String title, image, date, content;

    public Post() {
    }

    public Post(int id, String title, String image, String date, String content) {
        this.id = id;
        this.title = title;
        this.image = image;
        this.date = date;
        this.content = content;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = Utils.BitMapToString(image);
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getImageString() {
        return image;
    }
}