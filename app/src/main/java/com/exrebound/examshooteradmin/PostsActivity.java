package com.exrebound.examshooteradmin;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.exrebound.examshooteradmin.models.Category;
import com.exrebound.examshooteradmin.models.ErrorMessage;
import com.exrebound.examshooteradmin.utils.Parser;
import com.exrebound.examshooteradmin.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import info.androidhive.listviewfeed.data.Post;

public class PostsActivity extends AppCompatActivity implements NewPostDialog.OnPostPublishedListener {


    public static final String TAG = ".PostsActivity";
    public static final String KEY_CATEGORY_ID = ".category_id";
    public static final String KEY_ID = ".id";
    public static final String KEY_CONTENT = ".content";
    public static final String KEY_DATE = ".date";
    public static final String KEY_TITLE = ".title";
    public static final String KEY_IMAGE = ".image";

    //============================================================================================//
    //                                      UI References                                         //
    //============================================================================================//
    private ListView postsListView;
    private TextView emptyTextView;
    private LinearLayout containerLinearLayout;
    private ProgressBar mProgressView;

    //============================================================================================//
    //                                  References Type Variables                                 //
    //============================================================================================//
    private FragmentManager fm;
    private RequestQueue queue;
    private FeedListAdapter adapter;

    //============================================================================================//
    //                                      Type Variables                                        //
    //============================================================================================//
    private ArrayList<Post> posts;
    private ArrayList<Category> categories;
    private int categoryID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_posts);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        fm = getSupportFragmentManager();
        categoryID = getIntent().getIntExtra(CategoriesActivity.EXTRA_CATEGORY_ID,0);

        prepareUI();
        getCategories();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NewPostDialog d = new NewPostDialog();
                Bundle b = new Bundle();
                b.putInt(KEY_CATEGORY_ID, categoryID);
                d.setArguments(b);
                d.show(fm, "New Post");
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void prepareUI() {
        containerLinearLayout = (LinearLayout) findViewById(R.id.LLContainer);
        mProgressView = (ProgressBar) findViewById(R.id.progressBar);
        mProgressView.setVisibility(View.GONE);
        postsListView = (ListView) findViewById(R.id.LVPosts);
        emptyTextView = (TextView) findViewById(R.id.empty);
        postsListView.setEmptyView(emptyTextView);
        getPosts();
        registerForContextMenu(postsListView);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle("Options");
        menu.add(0, v.getId(), 0, "Edit");
        menu.add(0, v.getId(), 0, "Delete");
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo acmi = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        Post p = (Post) postsListView.getItemAtPosition(acmi.position);
        if (item.getTitle() == "Delete") {
            deletePost(p);
        } else if (item.getTitle() == "Edit") {
            editPostDialog(p);
        } else {
            return false;
        }
        return true;
    }

    private void editPostDialog(Post p) {
        NewPostDialog d = new NewPostDialog();
        Bundle b = new Bundle();
        b.putInt(KEY_CATEGORY_ID, categoryID);
        b.putInt(KEY_ID, p.getId());
        b.putString(KEY_CONTENT, p.getContent());
        b.putString(KEY_DATE,p.getDate());
        b.putString(KEY_TITLE,p.getTitle());
        b.putString(KEY_IMAGE,p.getImageString());
        d.setArguments(b);
        d.show(fm, "New Post");
    }

    public void getCategories() {
        String URL = "http://everythinghere.in/app/index.php/category";
        if (Utils.isNetworkOnline(this)) {
            showProgress(true);
            StringRequest request = new StringRequest(Request.Method.GET, URL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {
                            showProgress(false);
                            categories = Parser.getCategories(s);
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            showProgress(false);
                            Utils.showLightSnackbar(containerLinearLayout, "Error Occurred While Fetching Questions");
                        }
                    });
            if(queue == null)
                queue = Volley.newRequestQueue(PostsActivity.this);
            queue.add(request);
        } else {
            showProgress(false);
            Utils.showLightSnackbar(containerLinearLayout, "Network Not Found!");
        }
    }

    private void deletePost(final Post p) {
        String URL = "http://everythinghere.in/app/index.php/post/delete/"+p.getId();

        if(Utils.isNetworkOnline(this)){
            StringRequest request = new StringRequest(Request.Method.POST, URL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {
                            showProgress(false);
                            ErrorMessage e = Parser.getError(s);
                            if(e != null) {
                                Utils.showLightSnackbar(containerLinearLayout, e.getMessage());
                                if(e.getSuccess() == 1){
                                    posts.remove(p);
                                    adapter.notifyDataSetChanged();
                                }
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            showProgress(false);
                            Utils.showLightSnackbar(containerLinearLayout, "Error Occurred While Deleting Category");
                        }
                    });
            if(queue == null)
                queue = Volley.newRequestQueue(PostsActivity.this);
            queue.add(request);
        }else{
            showProgress(false);
            Utils.showLightSnackbar(containerLinearLayout, "Network Not Found!");
        }
    }

    @Override
    public void onPublish(boolean success, String message) {
        getPosts();
        Utils.showLightSnackbar(containerLinearLayout, message);
    }

    public void getPosts() {
        String URL = "http://everythinghere.in/app/index.php/post/category/"+categoryID;
        if (Utils.isNetworkOnline(this)) {
            showProgress(true);
            StringRequest request = new StringRequest(Request.Method.GET, URL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {
                            Log.e(TAG,s);
                            showProgress(false);
                            posts = Parser.getPosts(s);
                            if (posts != null && posts.size() > 0) {
                                adapter = new FeedListAdapter(PostsActivity.this, posts);
                                postsListView.setAdapter(adapter);
                            } else {
                                ErrorMessage e = Parser.getArrayError(s);
                                if (e != null)
                                    Utils.showLightSnackbar(containerLinearLayout, e.getMessage());
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            showProgress(false);
                            Utils.showLightSnackbar(containerLinearLayout, "Error Occurred While Fetching Questions");
                        }
                    });
            if(queue == null)
                queue = Volley.newRequestQueue(PostsActivity.this);
            queue.add(request);
        } else {
            showProgress(false);
            Utils.showLightSnackbar(containerLinearLayout, "Network Not Found!");
        }
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    public void showProgress(final boolean show) {

        // The ViewPropertyAnimator APIs are not available, so simply show
        // and hide the relevant UI components.
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    public ArrayList<Category> getCategoriesList() {
        return categories;
    }


    public class FeedListAdapter extends BaseAdapter {
        private static final String IMAGE_URL = "http://everythinghere.in/app/images/";
        private Activity activity;
        private LayoutInflater inflater;
        private List<Post> posts;
        Bitmap image;

        public FeedListAdapter(Activity activity, List<Post> posts) {
            this.activity = activity;
            this.posts = posts;
        }

        @Override
        public int getCount() {
            return posts.size();
        }

        @Override
        public Object getItem(int location) {
            return posts.get(location);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            if (inflater == null)
                inflater = (LayoutInflater) activity
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (convertView == null)
                convertView = inflater.inflate(R.layout.feed_item, null);

            TextView name = (TextView) convertView.findViewById(R.id.name);
            TextView timestamp = (TextView) convertView
                    .findViewById(R.id.timestamp);
            TextView statusMsg = (TextView) convertView
                    .findViewById(R.id.txtStatusMsg);
            ImageView feedImageView = (ImageView) convertView
                    .findViewById(R.id.feedImage1);

            Post item = posts.get(position);

            name.setText(item.getTitle());

            timestamp.setText(item.getDate());

            // Chcek for empty status message
            if (!TextUtils.isEmpty(item.getContent())) {
                statusMsg.setText(item.getContent());
                statusMsg.setVisibility(View.VISIBLE);
            } else {
                // status is empty, remove from view
                statusMsg.setVisibility(View.GONE);
            }

            // Feed image
            if (item.getImage() != null && item.getImage().length() > 0 && !item.getImage().equals("-")) {
                getImage(IMAGE_URL + item.getImage(), feedImageView);
            } else {
                feedImageView.setVisibility(View.GONE);
            }

            return convertView;
        }

        private void getImage(String url, final ImageView feedImageView) {
            url = url.replace(" ", "%20");
            Log.e(TAG,url);
            if (Utils.isNetworkOnline(PostsActivity.this)) {
                showProgress(true);
                ImageRequest request = new ImageRequest(url, new Response.Listener<Bitmap>() {
                    @Override
                    public void onResponse(Bitmap bitmap) {
                        showProgress(false);
                        Log.e(TAG,"Success!");
                        feedImageView.setImageBitmap(bitmap);
                        feedImageView.setVisibility(View.VISIBLE);
                    }
                }, 0, 0, null, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        showProgress(false);
                        Log.e(TAG,volleyError.toString());
                        Utils.showAlertDialog(PostsActivity.this,"Error", "Error Occurred while Downloading Image..!");
                    }
                });
                if(queue == null)
                    queue = Volley.newRequestQueue(PostsActivity.this);
                queue.add(request);
            } else {
                Utils.showAlertDialog(PostsActivity.this,"Error", "Network Not Found!");
            }
        }
    }
}