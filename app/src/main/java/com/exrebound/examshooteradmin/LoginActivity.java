package com.exrebound.examshooteradmin;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.exrebound.examshooteradmin.models.ErrorMessage;
import com.exrebound.examshooteradmin.utils.Parser;
import com.exrebound.examshooteradmin.utils.Utils;

public class LoginActivity extends AppCompatActivity {

    private static final String EXTRA_USERNAME = "com.exrebound.vehicletracking.username";
    private static final String EXTRA_PASSWORD = "com.exrebound.vehicletracking.password";
    public static String EXTRA_EMPLOYEE_ID = "com.exrebound.vehicletracking.employee_id";

    //============================================================================================//
    //                                      UI References                                         //
    //============================================================================================//
    private View mProgressView;
    private EditText usernameEditText, passwordEditText;
    private Button loginButton;
    private ImageView logoImageView;
    private LinearLayout container;

    //============================================================================================//
    //                                  References Type Variables                                 //
    //============================================================================================//
    private RequestQueue queue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // remove title
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // Set up the login form.
        container = (LinearLayout) findViewById(R.id.LLContainer);
        mProgressView = findViewById(R.id.login_progress);
        usernameEditText = (EditText) findViewById(R.id.ETUsername);
        passwordEditText = (EditText) findViewById(R.id.ETPassword);
        loginButton = (Button) findViewById(R.id.BLogIn);
        //Load Username from prefs
        String username = getSharedPreferences("Username", Context.MODE_PRIVATE).getString(EXTRA_USERNAME,"");
        usernameEditText.setText(username);
        // On Click listeners
        usernameEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgress(false);
            }
        });
        passwordEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgress(false);
            }
        });
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgress(true);
                String username = usernameEditText.getText().toString();
                String password = passwordEditText.getText().toString();
                if(username.isEmpty()){ usernameEditText.setError("Field Can't Be Left Empty"); return; }
                if(password.isEmpty()){ passwordEditText.setError("Field Can't Be Left Empty"); return; }
                attemptLogin(username, password);
            }
        });

        logoImageView = (ImageView) findViewById(R.id.IVLogo);

    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    public void attemptLogin(final String username, final String password) {
        String URL = "http://everythinghere.in/app/index.php/admin/"+username+"/"+password;
        if(Utils.isNetworkOnline(this)){
            StringRequest request = new StringRequest(Request.Method.GET, URL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {
                            String username = Parser.getAdminLoginResponse(s);
                            showProgress(false);
                            if(username != null){
                                Toast.makeText(LoginActivity.this, "Welcome. " + username + "!", Toast.LENGTH_LONG).show();
                                Intent i = new Intent(LoginActivity.this,MenuActivity.class);
                                String oldname = getSharedPreferences("Username",Context.MODE_PRIVATE).getString(EXTRA_USERNAME,"");
                                //If username is different from prefs' username
                                if(!username.equals(oldname)) {
                                    //Save Username to prefs
                                    getSharedPreferences("Username", Context.MODE_PRIVATE).edit().putString(EXTRA_USERNAME, username).commit();
                                }
                                //Save Password to prefs
                                getSharedPreferences("Password", Context.MODE_PRIVATE).edit().putString(EXTRA_PASSWORD, password).commit();
                                queue.cancelAll(new RequestQueue.RequestFilter() {
                                    @Override
                                    public boolean apply(Request<?> request) {
                                        return true;
                                    }
                                });
                                startActivity(i);
                                finish();
                            }else {
                                ErrorMessage e = Parser.getError(s);
                                if(e != null)
                                    Utils.showLightSnackbar(container, e.getMessage());
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            showProgress(false);
                            Utils.showLightSnackbar(container, "Error Occurred While Logging In...");
                        }
                    });
            queue = Volley.newRequestQueue(this);
            queue.add(request);
        }else{
            showProgress(false);
            Utils.showLightSnackbar(container, "Network Not Found!");
        }
    }


    /**
     * Shows the progress UI and hides the login form.
     */
    public void showProgress(final boolean show) {

        // The ViewPropertyAnimator APIs are not available, so simply show
        // and hide the relevant UI components.
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
    }
}
