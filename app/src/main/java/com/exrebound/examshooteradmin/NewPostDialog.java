package com.exrebound.examshooteradmin;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.exrebound.examshooteradmin.models.Category;
import com.exrebound.examshooteradmin.models.ErrorMessage;
import com.exrebound.examshooteradmin.utils.CustomRequest;
import com.exrebound.examshooteradmin.utils.Parser;
import com.exrebound.examshooteradmin.utils.Utils;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import info.androidhive.listviewfeed.data.Post;

/**
 * Created by hp sleek book on 11/25/2015.
 */
public class NewPostDialog extends DialogFragment implements View.OnClickListener {

    private static final int PICK_PHOTO_FOR_AVATAR = 1;
    private static final int MY_SOCKET_TIMEOUT_MS = 10000;
    private static final String TAG = ".new_post_dialog";
    //============================================================================================//
    //                                      UI References                                         //
    //============================================================================================//
    private EditText titleEditText, contentEditText;
    private ImageView photoImageView;
    private Button cancelButton, publishButton, photoButton;
    private LinearLayout progressLinearLayout;
    private ListView categoriesListView;

    //============================================================================================//
    //                                  References Type Variables                                 //
    //============================================================================================//
    private OnPostPublishedListener listener;
    private RequestQueue queue;
    private Post p;

    //============================================================================================//
    //                                      Type Variables                                        //
    //============================================================================================//
    boolean success = false;
    String title, content;
    Bitmap image;
    private ArrayList<Category> categories;
    private int post_id;
    private String imageURL, filename;
    private Uri ImageURI;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        listener = (OnPostPublishedListener) activity;
    }

    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {

        // the content
        final RelativeLayout root = new RelativeLayout(getActivity());
        root.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        // creating the fullscreen dialog
        final Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(root);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_new_post, null);
        setCancelable(false);

        categoriesListView = (ListView) v.findViewById(R.id.LVCategories);
        categories = ((PostsActivity) getActivity()).getCategoriesList();
        categoriesListView.setAdapter(new CategoriesAdapter(getActivity(), R.layout.categories_item, categories));

        titleEditText = (EditText) v.findViewById(R.id.ETTitle);
        contentEditText = (EditText) v.findViewById(R.id.ETContent);
        photoImageView = (ImageView) v.findViewById(R.id.IVPhoto);
        photoImageView.setVisibility(View.GONE);
        photoButton = (Button) v.findViewById(R.id.BTImage);
        cancelButton = (Button) v.findViewById(R.id.BTCancel);
        publishButton = (Button) v.findViewById(R.id.BTPublish);

        photoButton.setOnClickListener(this);
        cancelButton.setOnClickListener(this);
        publishButton.setOnClickListener(this);

        progressLinearLayout = (LinearLayout) v.findViewById(R.id.LLProgress);

        prepareIfEditMode();

        return v;
    }

    private void prepareIfEditMode() {
        Bundle arg = getArguments();
        if (arg != null) {
            post_id = getArguments().getInt(PostsActivity.KEY_ID);
            title = (getArguments().getString(PostsActivity.KEY_TITLE) == null) ? "" : getArguments().getString(PostsActivity.KEY_TITLE);
            content = (getArguments().getString(PostsActivity.KEY_CONTENT) == null) ? "" : getArguments().getString(PostsActivity.KEY_CONTENT);
            byte[] array = getArguments().getByteArray(PostsActivity.KEY_IMAGE);
            if (array != null && array.length > 0)
                image = BitmapFactory.decodeByteArray(array, 0, array.length);
            titleEditText.setText(title);
            contentEditText.setText(content);
            if (image != null) {
                photoImageView.setImageBitmap(image);
                photoImageView.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.BTCancel) {
            dismiss();
        } else if (id == R.id.BTPublish) {
            showProgress(true);
            title = titleEditText.getText().toString();
            if (title.isEmpty()) {
                titleEditText.setError("Field Can't Be Left Empty");
                return;
            }

            content = contentEditText.getText().toString();
            if (content.isEmpty()) {
                contentEditText.setError("Field Can't Be Left Empty");
                return;
            }

            if (photoImageView.getDrawable() != null) {
                image = ((BitmapDrawable) photoImageView.getDrawable()).getBitmap();
                new UploadImage().execute();
            }else{
                imageURL = "-";
                if (post_id == 0) addNewPost(title, imageURL, content);
                else editPost(post_id, title, imageURL, content);
            }

        } else if (id == R.id.BTImage) {
            openGallery();
        }
    }

    public String uploadFile(final Uri sourceFileUri) {
        String fileName = Utils.getRealPathFromURI(getActivity(), sourceFileUri);
        Log.e("NewPost", "File: " + fileName);
        HttpURLConnection conn;
        DataOutputStream dos;
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";
        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1 * 1024 * 1024;
        File sourceFile = new File(fileName);
        String response = null;

        if (!sourceFile.isFile()) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showProgress(false);
                    Utils.showAlertDialog(getActivity(), "File Not Found!", "Source File not exist :" + sourceFileUri);
                }
            });
        } else {
            try {
                // open a URL connection to the Servlet
                FileInputStream fileInputStream = new FileInputStream(sourceFile);
                URL url = new URL("http://everythinghere.in/app/index.php/upload/");

                // Open a HTTP  connection to  the URL
                conn = (HttpURLConnection) url.openConnection();
                conn.setDoInput(true); // Allow Inputs
                conn.setDoOutput(true); // Allow Outputs
                conn.setUseCaches(false); // Don't use a Cached Copy
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Connection", "Keep-Alive");
                conn.setRequestProperty("ENCTYPE", "multipart/form-data");
                conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
                conn.setRequestProperty("image", fileName);

                dos = new DataOutputStream(conn.getOutputStream());

                dos.writeBytes(twoHyphens + boundary + lineEnd);
                dos.writeBytes("Content-Disposition: form-data; name=\"image\";filename=\"" + fileName + "\"" + lineEnd);
                dos.writeBytes(lineEnd);

                // create a buffer of  maximum size
                bytesAvailable = fileInputStream.available();

                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                buffer = new byte[bufferSize];

                // read file and write it into form...
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                while (bytesRead > 0) {
                    dos.write(buffer, 0, bufferSize);
                    bytesAvailable = fileInputStream.available();
                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                }

                // send multipart form data necessary after file data...
                dos.writeBytes(lineEnd);
                dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

                // Responses from the server (code and message)
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                StringBuilder sb = new StringBuilder();
                String line;
                while ((line = br.readLine()) != null) {
                    sb.append(line + "\n");
                }
                br.close();
                response = sb.toString();

                //close the streams //
                fileInputStream.close();
                dos.flush();
                dos.close();
            } catch (MalformedURLException ex) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        showProgress(false);
                    }
                });
                ex.printStackTrace();
                Log.e("Upload file to server", "error: " + ex.getMessage(), ex);
            } catch (Exception e) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        showProgress(false);
                    }
                });
                e.printStackTrace();
            }
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showProgress(false);
                }
            });
        } // End else block

        return response;
    }

    private File getImageFromBitmap(Bitmap image) {
        //create a file to write bitmap data
        File f = new File(getActivity().getCacheDir(), filename);
        try {
            f.createNewFile();

            //Convert bitmap to byte array
            Bitmap bitmap = image;
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos);
            byte[] bitmapdata = bos.toByteArray();

            //write the bytes in file
            FileOutputStream fos = new FileOutputStream(f);
            fos.write(bitmapdata);
            fos.flush();
            fos.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return f;
    }

    private void editPost(int id, String title, String image, String content) {
        String URL = "http://everythinghere.in/app/index.php/post/edit";

        content = content.replace("'","\'");
        content = content.replace("\"", "\\\"");

        Map<String, String> params = new HashMap<>();
        params.put("id", id+"");
        params.put("image", image);
        params.put("title", title);
        params.put("content", content);

        Log.e(TAG, params.toString());

        if (Utils.isNetworkOnline(getActivity())) {
            CustomRequest request = new CustomRequest(Request.Method.POST, URL, params,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject object) {
                            Log.e("NewPost", object.toString());
                            showProgress(false);
                            ErrorMessage e = Parser.getError(object);
                            if (e != null) {
                                success =  true;
                                Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
                                if (e.getSuccess() == 1) {
                                    //Notify the host activity that quiz has successfully added to the database so that it should refresh the categories
                                    listener.onPublish(success, e.getMessage());
                                    dismiss();
                                } else {
                                    Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            showProgress(false);
                            Toast.makeText(getActivity(), "Error Occurred While Adding New Quiz", Toast.LENGTH_LONG).show();
                        }
                    });
            queue = Volley.newRequestQueue(getActivity());
            queue.add(request);
        } else {
            showProgress(false);
            Toast.makeText(getActivity(), "Network Not Found!", Toast.LENGTH_LONG).show();
        }
    }

    private void addNewPost(String title, String image, String content) {

        String URL = "http://everythinghere.in/app/index.php/post";

        content = content.replace("'","\'");
        content = content.replace("\"", "\\\"");

        Map<String, String> params = new HashMap<>();
        params.put("title", title);
        params.put("image", image);
        params.put("content", content);

        Log.e(TAG,params.toString());

        if (Utils.isNetworkOnline(getActivity())) {
            CustomRequest request = new CustomRequest(Request.Method.POST, URL, params,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject object) {
                            Log.e("NewPost", "Response: "+object.toString());
                            showProgress(false);
                            ErrorMessage e = Parser.getError(object);
                            if (e != null) {
                                Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
                                if (e.getSuccess() == 1) {
                                    success =  true;
                                    post_id = Integer.parseInt(e.getMessage());
                                    for (Category c : categories) {
                                        if (c.isMarked())
                                            addPostInCategory(post_id, c.getId());
                                    }
                                    listener.onPublish(success, e.getMessage());
                                    dismiss();
                                } else {
                                    Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            showProgress(false);
                            Toast.makeText(getActivity(), "Error Occurred While Adding New Post", Toast.LENGTH_LONG).show();
                        }
                    });
            queue = Volley.newRequestQueue(getActivity());
            queue.add(request);
        } else {
            showProgress(false);
            Toast.makeText(getActivity(), "Network Not Found!", Toast.LENGTH_LONG).show();
        }
    }

    private void addPostInCategory(int post_id, int category_id) {
        String URL = "http://everythinghere.in/app/index.php/post_category/" + post_id + "/" + category_id;
        URL = URL.replace(" ", "%20");
        Log.e("NewPost", "URL: " + URL);

        if (Utils.isNetworkOnline(getActivity())) {
            StringRequest request = new StringRequest(Request.Method.POST, URL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {
                            Log.e("NewPost", "AddPostInCategory Response: " + s);
                            showProgress(false);
                            ErrorMessage e = Parser.getError(s);
                            if (e != null) {
                                if (e.getSuccess() == 1) {
                                    //Notify the host activity that quiz has successfully added to the database so that it should refresh the categories
                                    // listener.onPublish(success, e.getMessage());
                                    Log.e("NewPost","Response Message: " + e.getMessage());
                                } else {
                                    Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            showProgress(false);
                            Toast.makeText(getActivity(), "Error Occurred While Registering Category", Toast.LENGTH_LONG).show();
                        }
                    });
            queue = Volley.newRequestQueue(getActivity());
            queue.add(request);
        } else {
            showProgress(false);
            Toast.makeText(getActivity(), "Network Not Found!", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    public void showProgress(final boolean show) {
        // The ViewPropertyAnimator APIs are not available, so simply show
        // and hide the relevant UI components.
        progressLinearLayout.setVisibility(show ? View.VISIBLE : View.GONE);
    }


    private void openGallery() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        startActivityForResult(intent, PICK_PHOTO_FOR_AVATAR);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_PHOTO_FOR_AVATAR && resultCode == Activity.RESULT_OK) {
            if (data == null) {
                //Display an error
                return;
            }
            try {
                ImageURI = data.getData();
                filename = Utils.getRealPathFromURI(getActivity(), ImageURI);
                filename = filename.substring(filename.lastIndexOf("/") + 1);
                filename = filename.replace(" ", "");
                InputStream inputStream = getActivity().getContentResolver().openInputStream(data.getData());
                Bitmap b = BitmapFactory.decodeStream(inputStream);
                if (b != null) {
                    photoImageView.setVisibility(View.VISIBLE);
                    photoImageView.setImageBitmap(b);
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    private class UploadImage extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            String r = uploadFile(ImageURI);
            return r;
        }

        @Override
        protected void onPostExecute(final String s) {
            super.onPostExecute(s);
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    ErrorMessage e = Parser.getArrayError(s);
                    if (e.getSuccess() == 1) imageURL = e.getMessage();
                    else imageURL = "-";
                    Log.e("NewPost", "ImageURL: " + imageURL);
                    if (post_id == 0) addNewPost(title, imageURL, content);
                    else editPost(post_id, title, imageURL, content);
                    progressLinearLayout.setVisibility(View.VISIBLE);
                }
            });
        }
    }

    public interface OnPostPublishedListener {
        void onPublish(boolean success, String message);
    }

    private class CategoriesAdapter extends ArrayAdapter<Category> {
        public CategoriesAdapter(Context context, int resource, ArrayList<Category> objects) {
            super(context, resource, objects);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null)
                convertView = getActivity().getLayoutInflater().inflate(R.layout.categories_item, null);

            final Category c = getItem(position);

            CheckBox cb = (CheckBox) convertView.findViewById(R.id.CBCategory);
            cb.setText(c.getName());
            cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    c.setIsMarked(isChecked);
                }
            });
            return convertView;
        }
    }
}
