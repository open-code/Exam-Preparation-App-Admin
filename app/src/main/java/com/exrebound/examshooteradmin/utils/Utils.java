package com.exrebound.examshooteradmin.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.Base64;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.exrebound.examshooteradmin.MainActivity;

import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Calendar;

/**
 * Created by hp sleek book on 6/27/2015.
 */
public class Utils {

    //Check if network is online
    public static boolean isNetworkOnline(Context c) {
        boolean status = false;
        try{
            ConnectivityManager cm = (ConnectivityManager) c.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getNetworkInfo(0);
            if (netInfo != null && netInfo.getState() == NetworkInfo.State.CONNECTED) {
                status = true;
            }else {
                netInfo = cm.getNetworkInfo(1);
                if(netInfo!=null && netInfo.getState() == NetworkInfo.State.CONNECTED)
                    status = true;
            }
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
        return status;
    }

    //Show a custom alert dialog
    public static void showAlertDialog(final Context c, String title, String message) {
        new AlertDialog.Builder(c).setTitle(title).setMessage(message).setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Activity a = (Activity) c;
                a.startActivity(new Intent(c, MainActivity.class));
                a.finish();
            }
        }).show();
    }

    public static void showAlertDialogWithoutCancel(final Context c, String title, String message) {
        new AlertDialog.Builder(c).setTitle(title).setMessage(message).setPositiveButton(android.R.string.ok, null).show();
    }

    public static void appendLog(String text) {
        File logFile = new File("sdcard/log.txt");
        if (!logFile.exists()) {
            try
            {
                logFile.createNewFile();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        } try {
            //BufferedWriter for performance, true to set append to file flag
            BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
            buf.append(Calendar.getInstance().getTime().toString() + " -> " + text);
            buf.newLine();
            buf.newLine();
            buf.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public static void showLightSnackbar(LinearLayout layout, String message) {
        Snackbar s = Snackbar.make(layout, message, Snackbar.LENGTH_SHORT);
        TextView tv = (TextView) s.getView().findViewById(android.support.design.R.id.snackbar_text);
        tv.setTextColor(Color.WHITE);
        s.show();
    }

    /**
     * @param bitmap
     * @return converting bitmap and return a string
     */
    public static String BitMapToString(Bitmap bitmap){
        ByteArrayOutputStream baos=new  ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG,50, baos);
        byte [] b=baos.toByteArray();
        String temp = Base64.encodeToString(b, Base64.URL_SAFE | Base64.NO_WRAP);
        return temp;
    }

    /**
     * @param encodedString
     * @return bitmap (from given string)
     */
    public static Bitmap StringToBitMap(String encodedString){
        try{
            byte [] encodeByte=Base64.decode(encodedString,Base64.DEFAULT);
            Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
            return bitmap;
        }catch(Exception e){
            e.getMessage();
            return null;
        }
    }

    public static String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = { MediaStore.Images.Media.DATA };
            cursor = context.getContentResolver().query(contentUri,  proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

        public static SpannableStringBuilder getMultiStyleSignUpText(String text, int textColor, int startIndext, int endIndex, boolean isBold, boolean isItalic) {
        SpannableStringBuilder sb = new SpannableStringBuilder(text);
        // Span to set text color to some RGB value
        ForegroundColorSpan fcs = new ForegroundColorSpan(textColor);
        StyleSpan bss;
        if(isBold && isItalic) {
            // Span to make text bold and italic
            bss = new StyleSpan(Typeface.BOLD_ITALIC);
        }else if(isBold) {
            // Span to make text bold
            bss = new StyleSpan(android.graphics.Typeface.BOLD);
        }else if(isItalic) {
            // Span to make text bold
            bss = new StyleSpan(Typeface.ITALIC);
        }else{
            bss = new StyleSpan(android.graphics.Typeface.BOLD);
        }
        // Set the text color for first 4 characters
        sb.setSpan(fcs, startIndext, endIndex, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        // make them also bold
        sb.setSpan(bss, startIndext, endIndex, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        return sb;
    }
}
