package com.exrebound.examshooteradmin.utils;

import com.exrebound.examshooteradmin.models.Category;
import com.exrebound.examshooteradmin.models.ErrorMessage;
import com.exrebound.examshooteradmin.models.Question;
import com.exrebound.examshooteradmin.models.Quiz;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import info.androidhive.listviewfeed.data.Post;

/**
 * Created by Zuhaib on 6/20/2015.
 */
public class Parser {

    public static ErrorMessage getError(String s) {
        try {
            JSONObject object = new JSONObject(s);
            return new ErrorMessage(object.getInt("success"), object.getString("response"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static ErrorMessage getError(JSONObject object) {
        try {
            return new ErrorMessage(object.getInt("success"), object.getString("response"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getAdminLoginResponse(String s) {
        try{
            JSONObject object = new JSONObject(s);
            if (object.getInt("success") == 1) {
                return object.getString("username");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static ArrayList<Quiz> getQuizzes(String s) {
        ArrayList<Quiz> quizzes = new ArrayList<>();
        try {
            JSONArray array = new JSONArray(s);
            for (int i = 0; i < array.length(); i++) {
                JSONObject object = (JSONObject) array.get(i);
                if (object.getInt("success") == 1) {
                    quizzes.add(new Quiz(object.getInt("id"), object.getString("title"),
                            object.getInt("duration"), object.getInt("questions"), object.getInt("endsIn")));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return quizzes;
    }

    public static ArrayList<Question> getQuestions(String s) {
        ArrayList<Question> questions = new ArrayList<>();
        try {
            JSONArray array = new JSONArray(s);
            for (int i = 0; i < array.length(); i++) {
                JSONObject object = (JSONObject) array.get(i);
                if (object.getInt("success") == 1) {
                    questions.add(new Question(object.getInt("id"), object.getInt("quiz_id"), object.getString("subject"),
                            object.getString("content"), object.getString("answer"), object.getString("option_1"),
                            object.getString("option_2"), object.getString("option_3"), object.getString("option_4"),
                            object.getString("option_5")));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return questions;
    }

    public static ArrayList<Category> getCategories(String s) {
        ArrayList<Category> categories = new ArrayList<>();
        try {
            JSONArray array = new JSONArray(s);
            for (int i = 0; i < array.length(); i++) {
                JSONObject object = (JSONObject) array.get(i);
                if (object.getInt("success") == 1) {
                    categories.add(new Category(object.getInt("category_id"), object.getString("name")));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return categories;
    }

    public static ArrayList<Post> getPosts(String s) {
        ArrayList<Post> posts = new ArrayList<>();
        try {
            JSONArray array = new JSONArray(s);
            for (int i = 0; i < array.length(); i++) {
                JSONObject object = (JSONObject) array.get(i);
                if (object.getInt("success") == 1) {
                    posts.add(new Post(object.getInt("id"), object.getString("title"), object.getString("image"),
                            object.getString("date_added"), object.getString("content")));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return posts;
    }

    public static ErrorMessage getArrayError(String s) {
        try {
            JSONArray array = new JSONArray(s);
            JSONObject object = (JSONObject) array.get(0);
            return new ErrorMessage(object.getInt("success"), object.getString("response"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
