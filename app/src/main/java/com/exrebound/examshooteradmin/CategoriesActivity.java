package com.exrebound.examshooteradmin;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.exrebound.examshooteradmin.models.Category;
import com.exrebound.examshooteradmin.models.ErrorMessage;
import com.exrebound.examshooteradmin.utils.Parser;
import com.exrebound.examshooteradmin.utils.Utils;

import java.util.ArrayList;

public class CategoriesActivity extends AppCompatActivity implements NewCategoryDialog.OnCategoryPublishedListener {

    public static final String TAG = ".CategoriesActivity";
    public static final String KEY_ID = ".id";
    public static final String KEY_NAME = ".quiz_id";
    public static final String EXTRA_CATEGORY_ID = ".category_id";

    //============================================================================================//
    //                                      UI References                                         //
    //============================================================================================//
    private ListView categoriesListView;
    private TextView emptyTextView;
    private LinearLayout containerLinearLayout;
    private ProgressBar mProgressView;

    //============================================================================================//
    //                                  References Type Variables                                 //
    //============================================================================================//
    private FragmentManager fm;
    private RequestQueue queue;
    private ArrayAdapter<Category> adapter;

    //============================================================================================//
    //                                      Type Variables                                        //
    //============================================================================================//


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        fm = getSupportFragmentManager();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NewCategoryDialog d = new NewCategoryDialog();
                d.show(fm, "NewCategory");
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        prepareUI();
    }

    private void prepareUI() {
        containerLinearLayout = (LinearLayout) findViewById(R.id.LLContainer);
        mProgressView = (ProgressBar) findViewById(R.id.progressBar);
        mProgressView.setVisibility(View.GONE);
        categoriesListView = (ListView) findViewById(R.id.LVCategories);
        emptyTextView = (TextView) findViewById(R.id.empty);
        categoriesListView .setEmptyView(emptyTextView);
        getCategories();
        categoriesListView .setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Category c = (Category) categoriesListView.getItemAtPosition(position);
                Intent i = new Intent(CategoriesActivity.this,PostsActivity.class);
                i.putExtra(EXTRA_CATEGORY_ID,c.getId());
                startActivity(i);
            }
        });
        registerForContextMenu(categoriesListView);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle("Options");
        menu.add(0, v.getId(), 0, "Edit");
        menu.add(0, v.getId(), 0, "Delete");
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo acmi = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        Category c = (Category) categoriesListView.getItemAtPosition(acmi.position);
        if (item.getTitle() == "Delete") {
            deleteCategory(c);
        } else if (item.getTitle() == "Edit") {
            editCategoryDialog(c);
        } else {
            return false;
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main_activity, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.MILogOut) {
            Intent intent = new Intent(this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void editCategoryDialog(Category c) {
        NewCategoryDialog d = new NewCategoryDialog();
        Bundle b = new Bundle();
        b.putInt(KEY_ID, c.getId());
        b.putString(KEY_NAME,c.getName());
        d.setArguments(b);
        d.show(fm, "New Category");
    }

    private void deleteCategory(final Category c) {
        String URL = "http://everythinghere.in/app/index.php/category_delete/"+c.getId();

        if(Utils.isNetworkOnline(this)){
            StringRequest request = new StringRequest(Request.Method.POST, URL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {
                            showProgress(false);
                            ErrorMessage e = Parser.getError(s);
                            if(e != null) {
                                Utils.showLightSnackbar(containerLinearLayout, e.getMessage());
                                if(e.getSuccess() == 1){
                                    adapter.remove(c);
                                }
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            showProgress(false);
                            Utils.showLightSnackbar(containerLinearLayout, "Error Occurred While Deleting Category");
                        }
                    });
            queue = Volley.newRequestQueue(CategoriesActivity.this);
            queue.add(request);
        }else{
            showProgress(false);
            Utils.showLightSnackbar(containerLinearLayout, "Network Not Found!");
        }
    }

    @Override
    public void onPublish(boolean success, String message) {
        getCategories();
        Utils.showLightSnackbar(containerLinearLayout, message);
    }

    public void getCategories() {
        String URL = "http://everythinghere.in/app/index.php/category";
        if (Utils.isNetworkOnline(this)) {
            showProgress(true);
            StringRequest request = new StringRequest(Request.Method.GET, URL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {
                            showProgress(false);
                            ArrayList<Category> categories = Parser.getCategories(s);
                            if (categories != null && categories.size() > 0) {
                                adapter = new ArrayAdapter<>(CategoriesActivity.this, android.R.layout.simple_list_item_1, categories);
                                categoriesListView.setAdapter(adapter);
                            } else {
                                ErrorMessage e = Parser.getError(s);
                                if (e != null)
                                    Utils.showLightSnackbar(containerLinearLayout, e.getMessage());
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            showProgress(false);
                            Utils.showLightSnackbar(containerLinearLayout, "Error Occurred While Fetching Questions");
                        }
                    });
            queue = Volley.newRequestQueue(CategoriesActivity.this);
            queue.add(request);
        } else {
            showProgress(false);
            Utils.showLightSnackbar(containerLinearLayout, "Network Not Found!");
        }
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    public void showProgress(final boolean show) {

        // The ViewPropertyAnimator APIs are not available, so simply show
        // and hide the relevant UI components.
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
    }
}
