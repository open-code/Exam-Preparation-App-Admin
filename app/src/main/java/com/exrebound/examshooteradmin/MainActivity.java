package com.exrebound.examshooteradmin;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.exrebound.examshooteradmin.models.ErrorMessage;
import com.exrebound.examshooteradmin.models.Quiz;
import com.exrebound.examshooteradmin.utils.Parser;
import com.exrebound.examshooteradmin.utils.Utils;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements NewQuizDialog.OnQuizPublishedListener {

    public static final String EXTRA_QUIZ_ID = ".quiz_id";
    public static final String TAG = ".MainActivity";
    public static final String KEY_ID = ".id";
    public static final String KEY_TITLE = ".title";
    public static final String KEY_DURATION = ".duration";
    public static final String KEY_QUESTIONS = ".questions";
    public static final String KEY_ENDS_IN = ".endsin";

    //============================================================================================//
    //                                      UI References                                         //
    //============================================================================================//
    private ListView quizzesListView;
    private TextView emptyTextView;
    private LinearLayout containerLinearLayout;
    private ProgressBar mProgressView;

    //============================================================================================//
    //                                  References Type Variables                                 //
    //============================================================================================//
    private FragmentManager fm;
    private RequestQueue queue;
    private ArrayAdapter<Quiz> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        fm = getSupportFragmentManager();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NewQuizDialog d = new NewQuizDialog();
                d.show(fm, "NewQuiz");
            }
        });

        prepareUI();
    }

    private void prepareUI() {
        containerLinearLayout = (LinearLayout) findViewById(R.id.LLContainer);
        mProgressView = (ProgressBar) findViewById(R.id.progressBar);
        mProgressView.setVisibility(View.GONE);
        quizzesListView = (ListView) findViewById(R.id.LVQuizes);
        emptyTextView = (TextView) findViewById(R.id.empty);
        quizzesListView.setEmptyView(emptyTextView);
        getQuizzes();
        quizzesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Quiz q = (Quiz) quizzesListView.getItemAtPosition(position);
                Intent i = new Intent(MainActivity.this, QuizActivity.class);
                i.putExtra(EXTRA_QUIZ_ID, q.getId());
                startActivity(i);
            }
        });
        registerForContextMenu(quizzesListView);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle("Options");
        menu.add(0, v.getId(), 0, "Edit");
        menu.add(0, v.getId(), 0, "Delete");
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo acmi = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        Quiz q = (Quiz) quizzesListView.getItemAtPosition(acmi.position);
        if (item.getTitle() == "Delete") {
            deleteQuiz(q);
        } else if (item.getTitle() == "Edit") {
            NewQuizDialog d = new NewQuizDialog();
            Bundle b = new Bundle();
            b.putInt(KEY_ID, q.getId());
            b.putString(KEY_TITLE, q.getTitle());
            b.putInt(KEY_DURATION, q.getDuration());
            b.putInt(KEY_QUESTIONS, q.getQuestions());
            b.putInt(KEY_ENDS_IN, q.getEndsIn());
            d.setArguments(b);
            d.show(fm, "NewQuiz");
        } else {
            return false;
        }
        return true;
    }

    private void deleteQuiz(final Quiz q) {
        String URL = "http://everythinghere.in/app/index.php/quiz/delete/" + q.getId();

        if (Utils.isNetworkOnline(this)) {
            StringRequest request = new StringRequest(Request.Method.POST, URL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {
                            showProgress(false);
                            ErrorMessage e = Parser.getError(s);
                            if (e != null) {
                                Utils.showLightSnackbar(containerLinearLayout, e.getMessage());
                                if (e.getSuccess() == 1) {
                                    adapter.remove(q);
                                }
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            showProgress(false);
                            Utils.showLightSnackbar(containerLinearLayout, "Error Occurred While Deleting Quiz");
                        }
                    });
            queue = Volley.newRequestQueue(MainActivity.this);
            queue.add(request);
        } else {
            showProgress(false);
            Utils.showLightSnackbar(containerLinearLayout, "Network Not Found!");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main_activity, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.MILogOut) {
            Intent intent = new Intent(this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPublish(boolean success, String message) {
        getQuizzes();
        Utils.showLightSnackbar(containerLinearLayout, message);
    }

    public void getQuizzes() {
        String URL = "http://everythinghere.in/app/index.php/quiz";
        if (Utils.isNetworkOnline(this)) {
            showProgress(true);
            StringRequest request = new StringRequest(Request.Method.GET, URL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {
                            showProgress(false);
                            ArrayList<Quiz> quizzes = Parser.getQuizzes(s);
                            showProgress(false);
                            if (quizzes != null && quizzes.size() > 0) {
                                adapter = new ArrayAdapter<>(MainActivity.this, android.R.layout.simple_list_item_1, quizzes);
                                quizzesListView.setAdapter(adapter);
                            } else {
                                ErrorMessage e = Parser.getError(s);
                                if (e != null)
                                    Utils.showLightSnackbar(containerLinearLayout, e.getMessage());
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            showProgress(false);
                            Utils.showLightSnackbar(containerLinearLayout, "Error Occurred While Logging In...");
                        }
                    });
            queue = Volley.newRequestQueue(MainActivity.this);
            queue.add(request);
        } else {
            showProgress(false);
            Utils.showLightSnackbar(containerLinearLayout, "Network Not Found!");
        }
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    public void showProgress(final boolean show) {

        // The ViewPropertyAnimator APIs are not available, so simply show
        // and hide the relevant UI components.
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
    }
}