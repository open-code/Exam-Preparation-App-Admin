/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exrebound.examshooteradmin.models;

/**
 *
 * @author hp sleek book
 */
public class ErrorMessage {

    private int success;
    private String message;

    public ErrorMessage(int success, String message) {
        this.success = success;
        this.message = message;
    }

    public int getSuccess() {
        return success;
    }

    public String getMessage() {
        return message;
    }
}
