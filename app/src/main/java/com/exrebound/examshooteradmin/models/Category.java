package com.exrebound.examshooteradmin.models;

/**
 * Created by hp sleek book on 11/25/2015.
 */
public class Category {
    int id;
    String name;
    boolean isMarked;

    public Category(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isMarked() {
        return isMarked;
    }

    public void setIsMarked(boolean isMarked) {
        this.isMarked = isMarked;
    }

    @Override
    public String toString() {
        return getName();
    }
}
