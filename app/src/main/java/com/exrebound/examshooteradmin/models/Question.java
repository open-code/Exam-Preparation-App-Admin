package com.exrebound.examshooteradmin.models;

/**
 * Created by hp sleek book on 11/19/2015.
 */
public class Question {

    private int id;
    private int quizID;
    private String subject = "";
    private String content = "";
    private String option1 = "";
    private String option2 = "";
    private String option3 = "";
    private String option4 = "";
    private String option5 = "";
    private String answer = "";

    public Question(int id, int quizID, String subject, String content, String answer) {
        this.id = id;
        this.quizID = quizID;
        this.subject = subject;
        this.content = content;
        this.answer = answer;
    }

    public Question(int id, int quizID, String subject, String content, String answer, String option1, String option2, String option3, String option4, String option5) {
        this.id = id;
        this.quizID = quizID;
        this.subject = subject;
        this.content = content;
        this.option1 = option1;
        this.option2 = option2;
        this.option3 = option3;
        this.option4 = option4;
        this.option5 = option5;
        this.answer = answer;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getQuizID() {
        return quizID;
    }

    public void setQuizID(int quizID) {
        this.quizID = quizID;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getOption1() {
        return option1;
    }

    public void setOption1(String option1) {
        this.option1 = option1;
    }

    public String getOption2() {
        return option2;
    }

    public void setOption2(String option2) {
        this.option2 = option2;
    }

    public String getOption3() {
        return option3;
    }

    public void setOption3(String option3) {
        this.option3 = option3;
    }

    public String getOption4() {
        return option4;
    }

    public void setOption4(String option4) {
        this.option4 = option4;
    }

    public String getOption5() {
        return option5;
    }

    public void setOption5(String option5) {
        this.option5 = option5;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    @Override
    public String toString() { return getContent();}

    public String toDetailedString() {
        return "Question{" +
                "answer='" + answer + '\'' +
                ", id=" + id +
                ", quizID=" + quizID +
                ", subject='" + subject + '\'' +
                ", content='" + content + '\'' +
                ", option1='" + option1 + '\'' +
                ", option2='" + option2 + '\'' +
                ", option3='" + option3 + '\'' +
                ", option4='" + option4 + '\'' +
                ", option5='" + option5 + '\'' +
                '}';
    }
}
