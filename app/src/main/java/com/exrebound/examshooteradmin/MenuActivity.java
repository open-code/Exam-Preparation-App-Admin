package com.exrebound.examshooteradmin;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

public class MenuActivity extends AppCompatActivity implements View.OnClickListener {

    LinearLayout quizLayout, postsLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        quizLayout = (LinearLayout) findViewById(R.id.IBQuizzes);
        postsLayout = (LinearLayout) findViewById(R.id.IBPosts);
        quizLayout.setOnClickListener(this);
        postsLayout.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if(id == R.id.IBQuizzes){
            startActivity(new Intent(this,MainActivity.class));
        }else if(id == R.id.IBPosts){
            startActivity(new Intent(this,CategoriesActivity.class));
        }
    }
}
