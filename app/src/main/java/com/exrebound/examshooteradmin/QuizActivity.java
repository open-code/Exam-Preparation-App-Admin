package com.exrebound.examshooteradmin;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.exrebound.examshooteradmin.models.ErrorMessage;
import com.exrebound.examshooteradmin.models.Question;
import com.exrebound.examshooteradmin.utils.Parser;
import com.exrebound.examshooteradmin.utils.Utils;

import java.util.ArrayList;

public class QuizActivity extends AppCompatActivity implements NewQuestionDialog.OnQuestionPublishedListener {

    public static final String TAG = ".QuizActivity";
    public static final String KEY_ID = ".id";
    public static final String KEY_QUIZ_ID = ".quiz_id";
    public static final String KEY_CONTENT = ".content";
    public static final String KEY_SUBJECT = ".subject";
    public static final String KEY_ANSWER = ".answer";
    public static final String KEY_OPTION_1 = ".op1";
    public static final String KEY_OPTION_2 = ".op2";
    public static final String KEY_OPTION_3 = ".op3";
    public static final String KEY_OPTION_4 = ".op4";
    public static final String KEY_OPTION_5 = ".op5";

    //============================================================================================//
    //                                      UI References                                         //
    //============================================================================================//
    private ListView questionsListView;
    private TextView emptyTextView;
    private LinearLayout containerLinearLayout;
    private ProgressBar mProgressView;

    //============================================================================================//
    //                                  References Type Variables                                 //
    //============================================================================================//
    private FragmentManager fm;
    private RequestQueue queue;
    private ArrayAdapter<Question> adapter;

    //============================================================================================//
    //                                      Type Variables                                        //
    //============================================================================================//
    private int quizID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        fm = getSupportFragmentManager();
        quizID = getIntent().getIntExtra(MainActivity.EXTRA_QUIZ_ID, 0);
        Log.e(TAG, "Quiz ID: "+quizID);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NewQuestionDialog d = new NewQuestionDialog();
                Bundle b = new Bundle();
                b.putInt(KEY_QUIZ_ID, quizID);
                d.setArguments(b);
                d.show(fm, "NewQuestion");
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        prepareUI();
    }

    private void prepareUI() {
        containerLinearLayout = (LinearLayout) findViewById(R.id.LLContainer);
        mProgressView = (ProgressBar) findViewById(R.id.progressBar);
        mProgressView.setVisibility(View.GONE);
        questionsListView = (ListView) findViewById(R.id.LVQuestions);
        emptyTextView = (TextView) findViewById(R.id.empty);
        questionsListView.setEmptyView(emptyTextView);
        if(quizID != 0) getQuestions(quizID);
        questionsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Question q = (Question) questionsListView.getItemAtPosition(position);
                editQuestionDialog(q);
            }
        });
        registerForContextMenu(questionsListView);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle("Options");
        menu.add(0, v.getId(), 0, "Edit");
        menu.add(0, v.getId(), 0, "Delete");
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo acmi = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        Question q = (Question) questionsListView.getItemAtPosition(acmi.position);
        if (item.getTitle() == "Delete") {
            deleteQuestion(q);
        } else if (item.getTitle() == "Edit") {
            editQuestionDialog(q);
        } else {
            return false;
        }
        return true;
    }

    private void editQuestionDialog(Question q) {
        NewQuestionDialog d = new NewQuestionDialog();
        Bundle b = new Bundle();
        b.putInt(KEY_ID, q.getId());
        b.putInt(KEY_QUIZ_ID, q.getQuizID());
        b.putString(KEY_CONTENT, q.getContent());
        b.putString(KEY_SUBJECT, q.getSubject());
        b.putString(KEY_ANSWER, q.getAnswer());
        b.putString(KEY_OPTION_1, q.getOption1());
        b.putString(KEY_OPTION_2, q.getOption2());
        b.putString(KEY_OPTION_3, q.getOption3());
        b.putString(KEY_OPTION_4, q.getOption4());
        b.putString(KEY_OPTION_5, q.getOption5());
        d.setArguments(b);
        d.show(fm, "New Question");
    }

    private void deleteQuestion(final Question q) {
        String URL = "http://everythinghere.in/app/index.php/question/delete/"+q.getId();

        if(Utils.isNetworkOnline(this)){
            StringRequest request = new StringRequest(Request.Method.POST, URL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {
                            showProgress(false);
                            ErrorMessage e = Parser.getError(s);
                            if(e != null) {
                                Utils.showLightSnackbar(containerLinearLayout, e.getMessage());
                                if(e.getSuccess() == 1){
                                    adapter.remove(q);
                                }
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            showProgress(false);
                            Utils.showLightSnackbar(containerLinearLayout, "Error Occurred While Deleting question");
                        }
                    });
            queue = Volley.newRequestQueue(QuizActivity.this);
            queue.add(request);
        }else{
            showProgress(false);
            Utils.showLightSnackbar(containerLinearLayout, "Network Not Found!");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main_activity, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.MILogOut) {
            Intent intent = new Intent(this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPublish(boolean success, String message) {
        getQuestions(quizID);
        Utils.showLightSnackbar(containerLinearLayout, message);
    }

    public void getQuestions(int quizID) {
        String URL = "http://everythinghere.in/app/index.php/question/"+quizID;
        if (Utils.isNetworkOnline(this)) {
            showProgress(true);
            StringRequest request = new StringRequest(Request.Method.GET, URL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {
                            showProgress(false);
                            ArrayList<Question> questions = Parser.getQuestions(s);
                            if (questions != null && questions.size() > 0) {
                                adapter = new ArrayAdapter<>(QuizActivity.this, android.R.layout.simple_list_item_1, questions);
                                questionsListView.setAdapter(adapter);
                            } else {
                                ErrorMessage e = Parser.getError(s);
                                if (e != null)
                                    Utils.showLightSnackbar(containerLinearLayout, e.getMessage());
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            showProgress(false);
                            Utils.showLightSnackbar(containerLinearLayout, "Error Occurred While Fetching Questions");
                        }
                    });
            queue = Volley.newRequestQueue(QuizActivity.this);
            queue.add(request);
        } else {
            showProgress(false);
            Utils.showLightSnackbar(containerLinearLayout, "Network Not Found!");
        }
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    public void showProgress(final boolean show) {

        // The ViewPropertyAnimator APIs are not available, so simply show
        // and hide the relevant UI components.
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
    }
}
