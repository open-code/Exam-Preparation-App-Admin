package com.exrebound.examshooteradmin;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.exrebound.examshooteradmin.models.Category;
import com.exrebound.examshooteradmin.models.ErrorMessage;
import com.exrebound.examshooteradmin.models.Question;
import com.exrebound.examshooteradmin.utils.Parser;
import com.exrebound.examshooteradmin.utils.Utils;

/**
 * Created by hp sleek book on 11/25/2015.
 */
public class NewCategoryDialog  extends DialogFragment implements View.OnClickListener {

    //============================================================================================//
    //                                      UI References                                         //
    //============================================================================================//
    private EditText questionEditText;
    private Button cancelButton, publishButton;
    private LinearLayout progressLinearLayout;

    //============================================================================================//
    //                                  References Type Variables                                 //
    //============================================================================================//
    private OnCategoryPublishedListener listener;
    private RequestQueue queue;
    private Category c;

    //============================================================================================//
    //                                      Type Variables                                        //
    //============================================================================================//
    boolean success = false;
    String name;
    int category_id;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        listener = (OnCategoryPublishedListener) activity;
    }

    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {

        // the content
        final RelativeLayout root = new RelativeLayout(getActivity());
        root.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        // creating the fullscreen dialog
        final Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(root);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_new_category, null);
        setCancelable(false);

        questionEditText = (EditText) v.findViewById(R.id.ETCategory);
        cancelButton = (Button) v.findViewById(R.id.BTCancel);
        publishButton = (Button) v.findViewById(R.id.BTPublish);

        cancelButton.setOnClickListener(this);
        publishButton.setOnClickListener(this);

        progressLinearLayout = (LinearLayout) v.findViewById(R.id.LLProgress);

        prepareIfEditMode();

        return v;
    }

    private void prepareIfEditMode() {
        Bundle arg = getArguments();
        if(arg != null) {
            category_id = getArguments().getInt(CategoriesActivity.KEY_ID);
            name = getArguments().getString(CategoriesActivity.KEY_NAME);
            if (name == null) name = "";
            c = new Category(category_id, name);
            questionEditText.setText(name);
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.BTCancel) {
            dismiss();
        } else if (id == R.id.BTPublish) {
            name = questionEditText.getText().toString();
            if (name.isEmpty()) { questionEditText.setError("Field Can't Be Left Empty"); return; }

            if(this.category_id == 0)
                addNewCategory(name);
            else
                editCategory(category_id,name);
            progressLinearLayout.setVisibility(View.VISIBLE);
        }
    }

    private void editCategory(int id, String name) {

        String URL = "http://everythinghere.in/app/index.php/category/" + id + "/" + name;
        URL = URL.replace(" ", "%20");

        if (Utils.isNetworkOnline(getActivity())) {
            StringRequest request = new StringRequest(Request.Method.POST, URL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {
                            showProgress(false);
                            ErrorMessage e = Parser.getError(s);
                            if (e != null) {
                                Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
                                if (e.getSuccess() == 1) {
                                    //Notify the host activity that quiz has successfully added to the database so that it should refresh the list
                                    listener.onPublish(success, e.getMessage());
                                    dismiss();
                                } else {
                                    Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            showProgress(false);
                            Toast.makeText(getActivity(), "Error Occurred While Adding New Quiz", Toast.LENGTH_LONG).show();
                        }
                    });
            queue = Volley.newRequestQueue(getActivity());
            queue.add(request);
        } else {
            showProgress(false);
            Toast.makeText(getActivity(), "Network Not Found!", Toast.LENGTH_LONG).show();
        }
    }

    private void addNewCategory(String name) {

        String URL = "http://everythinghere.in/app/index.php/category/" + name;
        URL = URL.replace(" ", "%20");

        if (Utils.isNetworkOnline(getActivity())) {
            StringRequest request = new StringRequest(Request.Method.POST, URL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {
                            showProgress(false);
                            ErrorMessage e = Parser.getError(s);
                            if (e != null) {
                                Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
                                if (e.getSuccess() == 1) {
                                    //Notify the host activity that quiz has successfully added to the database so that it should refresh the list
                                    listener.onPublish(success, e.getMessage());
                                    dismiss();
                                } else {
                                    Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            showProgress(false);
                            Toast.makeText(getActivity(), "Error Occurred While Adding New Quiz", Toast.LENGTH_LONG).show();
                        }
                    });
            queue = Volley.newRequestQueue(getActivity());
            queue.add(request);
        } else {
            showProgress(false);
            Toast.makeText(getActivity(), "Network Not Found!", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    public void showProgress(final boolean show) {
        // The ViewPropertyAnimator APIs are not available, so simply show
        // and hide the relevant UI components.
        progressLinearLayout.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    public interface OnCategoryPublishedListener {
        void onPublish(boolean success, String message);
    }
}
