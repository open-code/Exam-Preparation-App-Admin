package com.exrebound.examshooteradmin;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.exrebound.examshooteradmin.models.ErrorMessage;
import com.exrebound.examshooteradmin.models.Question;
import com.exrebound.examshooteradmin.utils.CustomRequest;
import com.exrebound.examshooteradmin.utils.Parser;
import com.exrebound.examshooteradmin.utils.Utils;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by hp sleek book on 11/19/2015.
 */
public class NewQuestionDialog extends DialogFragment implements View.OnClickListener {

    private static final String TAG = ".NewQuestionDialog";
    //============================================================================================//
    //                                      UI References                                         //
    //============================================================================================//
    private EditText questionEditText, subjectEditText, answerEditText, option1EditText, option2EditText,
            option3EditText, option4EditText, option5EditText;
    private CheckBox isMCQCheckBox;
    private Button cancelButton, publishButton;
    private LinearLayout progressLinearLayout, optionsLinearLayout;

    //============================================================================================//
    //                                  References Type Variables                                 //
    //============================================================================================//
    private OnQuestionPublishedListener listener;
    private RequestQueue queue;
    private Question q;

    //============================================================================================//
    //                                      Type Variables                                        //
    //============================================================================================//
    boolean success = false, isMCQ = false;
    String content, subject, answer, optionOne, optionTwo, optionThree, optionFour, optionFive;
    int quizID, questionID;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        listener = (OnQuestionPublishedListener) activity;
    }

    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {

        // the content
        final RelativeLayout root = new RelativeLayout(getActivity());
        root.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        // creating the fullscreen dialog
        final Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(root);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_new_question, null);
        setCancelable(false);

        optionsLinearLayout = (LinearLayout) v.findViewById(R.id.LLOptions);
        optionsLinearLayout.setVisibility(View.GONE);
        questionEditText = (EditText) v.findViewById(R.id.ETQuestion);
        subjectEditText = (EditText) v.findViewById(R.id.ETSubject);
        answerEditText = (EditText) v.findViewById(R.id.ETAnswer);
        option1EditText = (EditText) v.findViewById(R.id.ETOption1);
        option2EditText = (EditText) v.findViewById(R.id.ETOption2);
        option3EditText = (EditText) v.findViewById(R.id.ETOption3);
        option4EditText = (EditText) v.findViewById(R.id.ETOption4);
        option5EditText = (EditText) v.findViewById(R.id.ETOption5);
        isMCQCheckBox = (CheckBox) v.findViewById(R.id.CBIsMCQ);
        isMCQCheckBox.setChecked(isMCQ);
        cancelButton = (Button) v.findViewById(R.id.BTCancel);
        publishButton = (Button) v.findViewById(R.id.BTPublish);

        isMCQCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                optionsLinearLayout.setVisibility(isChecked ? View.VISIBLE : View.GONE);
                isMCQ = isChecked;
                if (!isMCQ) {
                    option1EditText.setText("");
                    option2EditText.setText("");
                    option3EditText.setText("");
                    option4EditText.setText("");
                    option5EditText.setText("");
                }
            }
        });
        cancelButton.setOnClickListener(this);
        publishButton.setOnClickListener(this);

        progressLinearLayout = (LinearLayout) v.findViewById(R.id.LLProgress);

        prepareIfEditMode();

        return v;
    }

    private void prepareIfEditMode() {
        Bundle arg = getArguments();
        if (arg != null) {
            questionID = getArguments().getInt(QuizActivity.KEY_ID);
            quizID = getArguments().getInt(QuizActivity.KEY_QUIZ_ID);
            content = getArguments().getString(QuizActivity.KEY_CONTENT);
            if (content == null) content = "";
            subject = getArguments().getString(QuizActivity.KEY_SUBJECT);
            if (subject == null) subject = "";
            answer = getArguments().getString(QuizActivity.KEY_ANSWER);
            if (answer == null) answer = "";
            optionOne = getArguments().getString(QuizActivity.KEY_OPTION_1);
            if (optionOne == null) optionOne = "";
            optionTwo = getArguments().getString(QuizActivity.KEY_OPTION_2);
            if (optionTwo == null) optionTwo = "";
            optionThree = getArguments().getString(QuizActivity.KEY_OPTION_3);
            if (optionThree == null) optionThree = "";
            optionFour = getArguments().getString(QuizActivity.KEY_OPTION_4);
            if (optionFour == null) optionFour = "";
            optionFive = getArguments().getString(QuizActivity.KEY_OPTION_5);
            if (optionFive == null) optionFive = "";

            q = new Question(questionID, quizID, subject, content, answer, optionOne, optionTwo, optionThree, optionFour, optionFive);
            questionEditText.setText(content);
            subjectEditText.setText(subject);
            answerEditText.setText(answer);
            if ((optionOne.isEmpty() || optionOne.equals("-")) && (optionTwo.isEmpty() || optionTwo.equals("-")) && (optionThree.isEmpty() || optionThree.equals("-"))) {
                isMCQ = false;
                isMCQCheckBox.setChecked(false);
                optionsLinearLayout.setVisibility(View.GONE);
            } else {
                isMCQ = true;
                isMCQCheckBox.setChecked(true);
                optionsLinearLayout.setVisibility(View.VISIBLE);
                option1EditText.setText(optionOne);
                option2EditText.setText(optionTwo);
                option3EditText.setText(optionThree);
                option4EditText.setText(optionFour);
                option5EditText.setText(optionFive);
            }
        }

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.BTCancel) {
            dismiss();
        } else if (id == R.id.BTPublish) {
            content = questionEditText.getText().toString();
            subject = subjectEditText.getText().toString();
            answer = answerEditText.getText().toString();

            if (content.isEmpty()) {
                questionEditText.setError("Field Can't Be Left Empty");
                return;
            }
            if (subject.isEmpty()) {
                subjectEditText.setError("Field Can't Be Left Empty");
                return;
            }
            if (answer.isEmpty()) {
                answerEditText.setError("Field Can't Be Left Empty");
                return;
            }

            if (isMCQ) {
                optionOne = option1EditText.getText().toString();
                optionTwo = option2EditText.getText().toString();
                optionThree = option3EditText.getText().toString();
                optionFour = option4EditText.getText().toString();
                optionFive = option5EditText.getText().toString();

                if (optionOne.isEmpty()) {
                    option1EditText.setError("Field Can't Be Left Empty");
                    return;
                }
                if (optionTwo.isEmpty()) {
                    option2EditText.setError("Field Can't Be Left Empty");
                    return;
                }
                if (optionThree.isEmpty()) {
                    option3EditText.setError("Field Can't Be Left Empty");
                    return;
                }
                if (optionFour.isEmpty()) {
                    option4EditText.setError("Field Can't Be Left Empty");
                    return;
                }
                if (optionFive.isEmpty()) {
                    optionFive = "-";
                }

            }

            if (optionOne.isEmpty()) optionOne = "-";
            if (optionTwo.isEmpty()) optionTwo = "-";
            if (optionThree.isEmpty()) optionThree = "-";
            if (optionFour.isEmpty()) optionFour = "-";
            if (optionFive.isEmpty()) optionFive = "-";

            if (questionID == 0)
                addNewQuestion(quizID, content, subject, answer, optionOne, optionTwo, optionThree, optionFour, optionFive);
            else
                editQuestion(questionID, content, subject, answer, optionOne, optionTwo, optionThree, optionFour, optionFive);
            progressLinearLayout.setVisibility(View.VISIBLE);

        }
    }

    private void editQuestion(final int id, String content, final String subject, String answer, String optionOne,
                              String optionTwo, String optionThree, String optionFour, String optionFive) {

        String URL = "http://everythinghere.in/app/index.php/question/edit";

        content = content.replace("'","\'");
        content = content.replace("\"", "\\\"");
        answer = answer.replace("'","\'");
        answer = answer.replace("\"", "\\\"");
        optionOne = optionOne.replace("'","\'");
        optionOne = optionOne.replace("\"", "\\\"");
        optionTwo = optionTwo.replace("'","\'");
        optionTwo = optionTwo.replace("\"", "\\\"");
        optionThree = optionThree.replace("'","\'");
        optionThree = optionThree.replace("\"", "\\\"");
        optionFour = optionFour.replace("'","\'");
        optionFour = optionFour.replace("\"", "\\\"");
        optionFive = optionFive.replace("'","\'");
        optionFive = optionFive.replace("\"", "\\\"");

        Map<String, String> params = new HashMap<>();
        params.put("id", id + "");
        params.put("content", content);
        params.put("subject", subject);
        params.put("answer", answer);
        params.put("option1", optionOne);
        params.put("option2", optionTwo);
        params.put("option3", optionThree);
        params.put("option4", optionFour);
        params.put("option5", optionFive);

        if (Utils.isNetworkOnline(getActivity())) {
            CustomRequest request = new CustomRequest(Request.Method.POST, URL, params,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject object) {
                            showProgress(false);
                            ErrorMessage e = Parser.getError(object);
                            if (e != null) {
                                Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
                                if (e.getSuccess() == 1) {
                                    //Notify the host activity that quiz has successfully added to the database so that it should refresh the list
                                    listener.onPublish(success, e.getMessage());
                                    dismiss();
                                } else {
                                    Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            showProgress(false);
                            Toast.makeText(getActivity(), "Error Occurred While Adding New Quiz", Toast.LENGTH_LONG).show();
                        }
                    });
            queue = Volley.newRequestQueue(getActivity());
            queue.add(request);
        } else {
            showProgress(false);
            Toast.makeText(getActivity(), "Network Not Found!", Toast.LENGTH_LONG).show();
        }
    }

    private void addNewQuestion(final int quizID, String content, final String subject, String answer, String optionOne,
                                String optionTwo, String optionThree, String optionFour, String optionFive) {

        String URL = "http://everythinghere.in/app/index.php/question";

        content = content.replace("'","\'");
        content = content.replace("\"", "\\\"");
        answer = answer.replace("'","\'");
        answer = answer.replace("\"", "\\\"");
        optionOne = optionOne.replace("'","\'");
        optionOne = optionOne.replace("\"", "\\\"");
        optionTwo = optionTwo.replace("'","\'");
        optionTwo = optionTwo.replace("\"", "\\\"");
        optionThree = optionThree.replace("'","\'");
        optionThree = optionThree.replace("\"", "\\\"");
        optionFour = optionFour.replace("'","\'");
        optionFour = optionFour.replace("\"", "\\\"");
        optionFive = optionFive.replace("'","\'");
        optionFive = optionFive.replace("\"", "\\\"");

        Map<String, String> params = new HashMap<>();
        params.put("quizID", quizID + "");
        params.put("content", content);
        params.put("subject", subject);
        params.put("answer", answer);
        params.put("option1", optionOne);
        params.put("option2", optionTwo);
        params.put("option3", optionThree);
        params.put("option4", optionFour);
        params.put("option5", optionFive);


        if (Utils.isNetworkOnline(getActivity())) {
            CustomRequest request = new CustomRequest(Request.Method.POST, URL, params,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject object) {
                            showProgress(false);
                            ErrorMessage e = Parser.getError(object);
                            if (e != null) {
                                Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
                                if (e.getSuccess() == 1) {
                                    //Notify the host activity that quiz has successfully added to the database so that it should refresh the list
                                    listener.onPublish(success, e.getMessage());
                                    dismiss();
                                } else {
                                    Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            showProgress(false);
                            Toast.makeText(getActivity(), "Error Occurred While Adding New Quiz", Toast.LENGTH_LONG).show();
                        }
                    });
            queue = Volley.newRequestQueue(getActivity());
            queue.add(request);
        } else {
            showProgress(false);
            Toast.makeText(getActivity(), "Network Not Found!", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    public void showProgress(final boolean show) {
        // The ViewPropertyAnimator APIs are not available, so simply show
        // and hide the relevant UI components.
        progressLinearLayout.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    public interface OnQuestionPublishedListener {
        void onPublish(boolean success, String message);
    }
}