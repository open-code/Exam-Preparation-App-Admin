package com.exrebound.examshooteradmin;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.exrebound.examshooteradmin.models.ErrorMessage;
import com.exrebound.examshooteradmin.models.Quiz;
import com.exrebound.examshooteradmin.utils.Parser;
import com.exrebound.examshooteradmin.utils.Utils;

/**
 * Created by hp sleek book on 11/16/2015.
 */
public class NewQuizDialog extends DialogFragment implements View.OnClickListener {

    //============================================================================================//
    //                                      UI References                                         //
    //============================================================================================//
    private EditText titleEditText, durationEditText, questionsEditText, endsInEditText;
    private Button cancelButton, publishButton;
    private LinearLayout progressLinearLayout;

    //============================================================================================//
    //                                  References Type Variables                                 //
    //============================================================================================//
    private OnQuizPublishedListener listener;
    private RequestQueue queue;
    private Quiz q;

    //============================================================================================//
    //                                      Type Variables                                        //
    //============================================================================================//
    boolean success = false;
    String title = "";
    int duration, questions, endsIn, id;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        listener = (OnQuizPublishedListener) activity;
    }

    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {

        // the content
        final RelativeLayout root = new RelativeLayout(getActivity());
        root.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        // creating the fullscreen dialog
        final Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(root);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dialog_new_quiz, null);
        setCancelable(false);
        titleEditText = (EditText) v.findViewById(R.id.ETTitle);
        durationEditText = (EditText) v.findViewById(R.id.ETDuration);
        questionsEditText = (EditText) v.findViewById(R.id.ETQuestions);
        endsInEditText = (EditText) v.findViewById(R.id.ETEndsIn);
        cancelButton = (Button) v.findViewById(R.id.BTCancel);
        publishButton = (Button) v.findViewById(R.id.BTPublish);

        cancelButton.setOnClickListener(this);
        publishButton.setOnClickListener(this);
        progressLinearLayout = (LinearLayout) v.findViewById(R.id.LLProgress);

        prepareIfEditMode();

        return v;
    }

    private void prepareIfEditMode() {
        Bundle arg = getArguments();
        if(arg != null) {
            id = getArguments().getInt(MainActivity.KEY_ID);
            title = getArguments().getString(MainActivity.KEY_TITLE);
            duration = getArguments().getInt(MainActivity.KEY_DURATION);
            questions = getArguments().getInt(MainActivity.KEY_QUESTIONS);
            endsIn = getArguments().getInt(MainActivity.KEY_ENDS_IN);
        }
        q = new Quiz(id, title, duration, questions, endsIn);
        Log.e(MainActivity.TAG, q.toString());
        if (q.getId() != 0) {
            titleEditText.setText(q.getTitle());
            durationEditText.setText(q.getDuration() + "");
            questionsEditText.setText(q.getQuestions() + "");
            endsInEditText.setText(q.getEndsIn() + "");
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.BTCancel) {
            dismiss();
        } else if (id == R.id.BTPublish) {
            title = titleEditText.getText().toString();
            String durationString = durationEditText.getText().toString();
            String questionsString = questionsEditText.getText().toString();
            String endsInString = endsInEditText.getText().toString();

            if (title.isEmpty()) {
                titleEditText.setError("Field Can't Be Left Empty");
                return;
            }
            if (durationString.isEmpty()) {
                durationEditText.setError("Field Can't Be Left Empty");
                return;
            } else duration = Integer.parseInt(durationString);

            if (questionsString.isEmpty()) {
                questionsEditText.setError("Field Can't Be Left Empty");
                return;
            } else questions = Integer.parseInt(questionsString);

            if (endsInString.isEmpty()) {
                endsInEditText.setError("Field Can't Be Left Empty");
                return;
            } else endsIn = Integer.parseInt(endsInString);

            if (q.getId() != 0)
                editQuiz(q.getId(), title, duration, questions, endsIn);
            else
                addNewQuiz(title, duration, questions, endsIn);
            progressLinearLayout.setVisibility(View.VISIBLE);
        }
    }

    private void editQuiz(int id, String title, int duration, int questions, int endsIn) {
        String URL = "http://everythinghere.in/app/index.php/quiz/" + id + "/" + title + "/" + duration + "/" + questions + "/" + endsIn;
        URL = URL.replace(" ", "%20");

        if (Utils.isNetworkOnline(getActivity())) {
            StringRequest request = new StringRequest(Request.Method.POST, URL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {
                            showProgress(false);
                            ErrorMessage e = Parser.getError(s);
                            if (e != null) {
                                Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
                                if (e.getSuccess() == 1) {
                                    //Notify the host activity that quiz has successfully added to the database so that it should refresh the list
                                    listener.onPublish(success, e.getMessage());
                                    dismiss();
                                }else{
                                    Toast.makeText(getActivity(),e.getMessage(),Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            showProgress(false);
                            Toast.makeText(getActivity(), "Error Occurred While Editing Quiz", Toast.LENGTH_LONG).show();
                        }
                    });
            queue = Volley.newRequestQueue(getActivity());
            queue.add(request);
        } else {
            showProgress(false);
            Toast.makeText(getActivity(), "Network Not Found!", Toast.LENGTH_LONG).show();
        }
    }

    private void addNewQuiz(String title, int duration, int questions, int endsIn) {
        String URL = "http://everythinghere.in/app/index.php/quiz/" + title + "/" + duration + "/" + questions + "/" + endsIn;
        URL = URL.replace(" ", "%20");

        if (Utils.isNetworkOnline(getActivity())) {
            StringRequest request = new StringRequest(Request.Method.POST, URL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String s) {
                            showProgress(false);
                            ErrorMessage e = Parser.getError(s);
                            if (e != null) {
                                Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
                                if (e.getSuccess() == 1) {
                                    //Notify the host activity that quiz has successfully added to the database so that it should refresh the list
                                    listener.onPublish(success, e.getMessage());
                                    dismiss();
                                }else{
                                    Toast.makeText(getActivity(),e.getMessage(),Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError volleyError) {
                            showProgress(false);
                            Toast.makeText(getActivity(), "Error Occurred While Adding New Quiz", Toast.LENGTH_LONG).show();
                        }
                    });
            queue = Volley.newRequestQueue(getActivity());
            queue.add(request);
        } else {
            showProgress(false);
            Toast.makeText(getActivity(), "Network Not Found!", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    public void showProgress(final boolean show) {
        // The ViewPropertyAnimator APIs are not available, so simply show
        // and hide the relevant UI components.
        progressLinearLayout.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    interface OnQuizPublishedListener {
        void onPublish(boolean success, String message);
    }
}
